package tcp

import (
	"log"
	"net"
	"os"
)

var (
	// ServerInfo Logger
	// valid information collection
	ServerInfo *log.Logger

	// ServerWarning Logger
	// not critical errors
	ServerWarning *log.Logger

	// ServerError Logger
	// critical errors
	ServerError *log.Logger

	// LogFile Logger
	// this is the log file
	serverLogFile *os.File
)

// Server : TCP server struct
type Server struct {
	mConnection net.Conn
	mListener   net.Listener
}

// initServerLogs function
// initalize all logs
func initServerLogs() {
	serverLogFile, err := os.OpenFile("server_log.log",
		os.O_CREATE|os.O_WRONLY|os.O_APPEND,
		0666)

	if err != nil {
		log.Fatal(err)
	}

	if ServerInfo == nil {
		ServerInfo = log.New(serverLogFile,
			"Info: ",
			log.Ldate|log.Ltime|log.Lshortfile)
	}

	if ServerError == nil {
		ServerError = log.New(serverLogFile,
			"Error: ",
			log.Ldate|log.Ltime|log.Lshortfile)
	}

	if ServerWarning == nil {
		ServerWarning = log.New(serverLogFile,
			"Warning: ",
			log.Ldate|log.Ltime|log.Lshortfile)
	}
}

// closeAllLogs close all logs
func closeAllServerLogs() {
	if serverLogFile != nil {
		name := serverLogFile.Name()
		ServerInfo.Print("close log file ", name)
	}
}

/*
CreateTCPServer :
@Description:
	Create tcp server
@Return:
*/
func CreateTCPServer() *Server {
	initServerLogs()
	return &Server{}
}

/*
InitServer :
@Description:
	This Server struct method is initialize the Server struct
@Return:
	[out] error - return error nil on success,
				  InvalidParamError, RegisterError on fail
*/
func (server *Server) InitServer() error {
	if server == nil {
		return &InvalidParamError{"Server is nil"}
	}

	ServerInfo.Print("Start collect server logs")
	ServerWarning.Print("Check warning Log")
	ServerError.Print("Check error Log")

	newListener, err := net.Listen("tcp", ":8081")
	if err != nil {
		ServerError.Print(err)
		return &RegisterError{"server"}
	}

	server.mListener = newListener
	return nil
}

/*
DestroyServer :
@Description:
	This Server struct method free all necessary data for the server
@Return:
	[out] error - return error nil on success,
				  InvalidParamError, CloseConnectionError on fail
*/
func (server *Server) DestroyServer() error {
	if server == nil {
		return &InvalidParamError{"Server is nil"}
	}

	var retError error
	ServerInfo.Print("Destroy TCP server")
	err := server.mListener.Close()

	if err != nil {
		ServerError.Print("TCP Close connection err ", err)
		retError = &CloseConnectionError{"server"}
	}

	closeAllServerLogs()
	return retError
}

/*
ServerListen :
@Description:
	This Server struct method start to listen to port that was connected.
	You should call this function only after InitServer succeed
@Return:
	[out] error - return error nil on success,
				  InvalidParamError, ConnectionError on fail
*/
func (server *Server) ServerListen() error {
	if server == nil {
		return &InvalidParamError{"Server is nil"}
	}
	conn, err := server.mListener.Accept()
	if err != nil {
		ServerError.Print(err)
		return &ConnectionError{"server"}
	}

	server.mConnection = conn
	return nil
}

/*
WaitForMessage :
@Description:
	This Server struct method blocking function wait to meesage from clientd.
@Return:
	[out] error - return error nil and refBuff on success,
				  InvalidParamError, ReadDataError on fail
*/
func (server *Server) WaitForMessage() (retBuff []byte, retErr error) {
	if server == nil {
		return nil, &InvalidParamError{"Server is nil"}
	}

	for {
		fixedSizeReadBuffer := make([]byte, 500)
		numberBytesRead, err := server.mConnection.Read(fixedSizeReadBuffer)
		if err != nil {
			ServerError.Print(err)
			retErr = &ReadDataError{"server"}
			return
		}

		ServerInfo.Print("Read Buffer size of ", numberBytesRead)
		if numberBytesRead != 0 {
			retBuff = append(retBuff, fixedSizeReadBuffer...)
		}

		if numberBytesRead < 500 {
			ServerInfo.Print("Final buffer size", len(retBuff))
			return
		}
	}
}
