package chat

import (
	"log"
	"os"
)

var (
	// ChatInfo Logger
	// valid information collection
	ChatInfo *log.Logger

	// ChatWarning Logger
	// not critical errors
	ChatWarning *log.Logger

	// ChatError Logger
	// critical errors
	ChatError *log.Logger

	// ChatLogFile Logger
	// this is the log file
	chatLogFile *os.File
)

// InitAllLogs init all logs for chat Chat
func initAllLogs() {
	var err error
	chatLogFile, err = os.OpenFile("chat_log.log",
		os.O_CREATE|os.O_WRONLY|os.O_APPEND,
		0666)

	if err != nil {
		log.Fatal(err)
	}

	if ChatInfo == nil {
		ChatInfo = log.New(chatLogFile,
			"Info: ",
			log.Ldate|log.Ltime|log.Lshortfile)
	}

	if ChatError == nil {
		ChatError = log.New(chatLogFile,
			"Error: ",
			log.Ldate|log.Ltime|log.Lshortfile)
	}

	if ChatWarning == nil {
		ChatWarning = log.New(chatLogFile,
			"Warning: ",
			log.Ldate|log.Ltime|log.Lshortfile)
	}
}

// DestroyLogs close all logs
func destroyLogs() {
	name := chatLogFile.Name()
	ChatInfo.Print("close log file ", name)
	chatLogFile.Close()
}
