package tcp

import (
	"log"
	"net"
	"os"
)

var (
	// ClientInfo Logger
	// valid information collection
	ClientInfo *log.Logger

	// ClientWarning Logger
	// not critical errors
	ClientWarning *log.Logger

	// ClientError Logger
	// critical errors
	ClientError *log.Logger

	// clientLogFile Logger
	// this is the log file
	clientLogFile *os.File
)

func initClientLogs() {
	clientLogFile, err := os.OpenFile("client_log.log",
		os.O_CREATE|os.O_WRONLY|os.O_APPEND,
		0666)

	if err != nil {
		log.Fatal(err)
	}

	if ClientInfo == nil {
		ClientInfo = log.New(clientLogFile,
			"Info: ",
			log.Ldate|log.Ltime|log.Lshortfile)
	}

	if ClientError == nil {
		ClientError = log.New(clientLogFile,
			"Error: ",
			log.Ldate|log.Ltime|log.Lshortfile)
	}

	if ClientWarning == nil {
		ClientWarning = log.New(clientLogFile,
			"Warning: ",
			log.Ldate|log.Ltime|log.Lshortfile)
	}
}

// closeAllLogs close all logs
func closeAllClientLogs() {
	var name string
	if clientLogFile != nil {
		name = clientLogFile.Name()
		if ClientInfo != nil {
			ClientInfo.Print("close log file ", name)
		}

		clientLogFile.Close()
	}

}

// Client TODO description
type Client struct {
	mConnection net.Conn
}

/*
CreateTCPClient :
@Description:
	Create tcp client
@Return:
	[out] Client - return error Client on success,
				  nil on failure
*/
func CreateTCPClient() *Client {
	initClientLogs()
	return &Client{}
}

/*
CreateTCPClient :
@Description:
	Create tcp client
@Return:
	[out] Client - return error Client on success,
				  nil on failure
*/
func (client *Client) DestroyTCPClient() {
	closeAllClientLogs()
}

/*
InitClient :
@Description:
	This Client struct method is initialize the Client struct
@Return:
	[out] error - return error nil on success,
				  InvalidParamError, ConnectionError on fail
*/
func (client *Client) InitClient() error {
	if client == nil {
		return &InvalidParamError{"Client is nil"}
	}
	initClientLogs()
	var err error
	client.mConnection, err = net.Dial("tcp", "127.0.0.1:8081")
	if err != nil {
		ClientError.Print(err)
		return &ConnectionError{"client"}
	}

	return nil
}

/*
SendData :
@Description:
@Return:
*/
func (client *Client) SendData(data []byte) (err error) {
	var bytesSend int
	if client == nil {
		err = &InvalidParamError{"Client is nil"}
		ClientError.Println(err)
		return
	}

	bytesSend, err = client.mConnection.Write(data)
	if err != nil {
		ClientError.Println(err)
		return
	}

	if bytesSend != len(data) {
		err = &SendAllDataError{"Client is nil", bytesSend}
		return
	}

	return
}
