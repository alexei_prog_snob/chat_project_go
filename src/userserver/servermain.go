package main

import (
	"chat"
	"fmt"
	"tcp"
)

func main() {
	var err error
	var messageFromClient []byte
	var serverManager *chat.ServerManager
	fmt.Println("Server Main")

	tcpServer := tcp.CreateTCPServer()
	defer tcpServer.DestroyServer()
	err = tcpServer.InitServer()
	if err != nil {
		fmt.Println(err)
		return
	}

	serverManager, err = chat.CreateServerManager()
	if err != nil {
		fmt.Println(err)
		return
	}
	defer serverManager.DestroyServerManager()

	err = tcpServer.ServerListen()
	if err != nil {
		fmt.Println(err)
		return
	}

	messageFromClient, err = tcpServer.WaitForMessage()
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(string(messageFromClient))
	serverManager.ParseRequestAndOperate(messageFromClient)

}
