package chat

// User :
// 	struct that holds all user information
type User struct {
	UserName    string `json:"name"`     // user's name
	Password    string `json:"password"` // user's password
	IsConnected bool   // indicate if user is connected
	IsAdmin     bool   // indicate if user is connected
}

// Users struct which contains
// an array of users
type Users struct {
	Users []User `json:"users"`
}

/*
CreateUser :
@Description:
	This function create a new User in your chat server
@Return:
	[out] newUser - return new User struct on success,
					return nil on fail
	[out] error - return error nil on success,
				  InvalidParamError on fail
*/
func CreateUser(userName, password string) (newUser *User, retErr error) {
	if len(userName) == 0 {
		retErr = &InvalidParamError{"userName invalid param"}
		return
	}

	if len(password) == 0 {
		retErr = &InvalidParamError{"password invalid param"}
		return
	}

	newUser = &User{
		UserName:    userName,
		Password:    password,
		IsConnected: false,
		IsAdmin:     false}
	return
}

/*
CreateAdmin :
@Description:
	This function create a new Admin User in your chat server
@Return:
	[out] newUser - return new User struct on success,
					return nil on fail
	[out] error - return error nil on success,
				  InvalidParamError on fail
*/
func CreateAdmin() (newUser *User, retErr error) {
	newUser = &User{
		UserName:    "Admin",
		Password:    "Admin123",
		IsConnected: false,
		IsAdmin:     true}
	return
}

/*
ValidatePassword :
@Description:
	This function User method that comapre the password
@Params:
	[in] password - password to check
@Return:
	[out] newUser - return new User struct on success,
					return nil on fail
	[out] error   - return error nil on success,
				    InvalidParamError on fail
*/
func (user *User) ValidatePassword(password string) bool {
	if user.Password == password {
		return true
	}
	return false
}

/*
GetUserName :
@Description:
	This function User method that return user's name
@Return:
	[out] string user's name
*/
func (user *User) GetUserName() string {
	return user.UserName
}

/*
GetPassword :
@Description:
	This function User method that return user's password
@Return:
	[out] string user's password
*/
func (user *User) GetPassword() string {
	return user.Password
}

/*
IsUserConnected :
@Description:
	This function User method that return is user is connected to chat
@Return:
	[out] string is connected
*/
func (user *User) IsUserConnected() bool {
	return user.IsConnected
}

/*
SetUserIsConnected :
@Description:
	This function User method that set that user is connected now
@Return:
*/
func (user *User) SetUserIsConnected(stat bool) {
	user.IsConnected = stat
}
