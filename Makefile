# Go related variables.
SRC:=./src


all: build_libs

build_libs: chat_lib tcp_lib server client

chat_lib:
	go build $(SRC)/chat

tcp_lib:
	go build $(SRC)/tcp

server:
	go install $(SRC)/userserver

client:
	go install $(SRC)/userclient

exec:
	export PATH=$$PATH:/usr/local/go/bin
	export GOPATH=$(shell pwd)

