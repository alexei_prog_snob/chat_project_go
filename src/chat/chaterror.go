package chat

import (
	"fmt"
)

// InvalidParamError this error return
// if you pass invalid param to tcp
type InvalidParamError struct {
	mErrorMsg string
}

func (err *InvalidParamError) Error() string {
	return fmt.Sprintf("Invalid param error: %s", err.mErrorMsg)
}

// ValidExitRequest this error is for user interface
// if there request to exit the chat screen
type ValidExitRequest struct {
	mErrorMsg string
	mToExit   bool
}

func (err *ValidExitRequest) Error() string {
	return fmt.Sprintf("Valid Exit Request from Client: %v", err.mToExit)
}
