package chat

import (
	"bytes"
	"encoding/gob"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

type ServerManager struct {
	mAllUsers map[string]*User
}

func (serverMng *ServerManager) initAllUsersFromJSON(fullJSONPath string) (retErr error) {
	if serverMng == nil {
		ChatError.Print("serverMng is nil")
		retErr = &InvalidParamError{"server manager is nil"}
		return
	}

	jsonFile, err := os.Open(fullJSONPath)
	if err != nil {
		ChatError.Print(err)
		retErr = err
		return
	}

	defer jsonFile.Close()

	// read our opened xmlFile as a byte array.
	var byteValue []byte
	byteValue, err = ioutil.ReadAll(jsonFile)
	if err != nil {
		ChatError.Print(err)
		retErr = err
		return
	}

	// we initialize our Users array
	var users Users

	// we unmarshal our byteArray which contains our
	// jsonFile's content into 'users' which we defined above
	json.Unmarshal(byteValue, &users)

	for i := 0; i < len(users.Users); i++ {
		serverMng.mAllUsers[users.Users[i].UserName], _ = CreateUser(users.Users[i].UserName, users.Users[i].Password)
	}
	return
}

/*
CreateServerManager :
@Description:
	This function create a new server manager in your chat server
@Return:
	[out] serverMng - return new ServerManager struct on success,
					return nil on fail
	[out] error - return error nil on success
*/
func CreateServerManager() (serverMng *ServerManager, retErr error) {
	serverMng = new(ServerManager)
	adminUser, err := CreateAdmin()
	if err != nil {
		ChatError.Print(err)
		retErr = err
		return nil, retErr
	}

	serverMng.mAllUsers = make(map[string]*User)
	serverMng.mAllUsers[adminUser.GetUserName()] = adminUser

	serverMng.initAllUsersFromJSON("./users.json")
	initAllLogs()
	return
}

/*
DestroyServerManager :
@Description:
@Return:
*/
func (serverMng *ServerManager) DestroyServerManager() {
	destroyLogs()
}

/*
ParseRequestAndOperate :
@Description:
	This ServerManager method parse the request buffer from client
@Return:
	[out] error - return error nil on success,
				  InvalidParamError on fail, gob errors
*/
func (serverMng *ServerManager) ParseRequestAndOperate(request []byte) (response []byte, retErr error) {
	if serverMng == nil {
		ChatError.Print("serverMng is nil")
		return nil, &InvalidParamError{"server manager is nil"}
	}

	requestType := new(RequestType)
	json.Unmarshal(request, &requestType)
	fmt.Println(requestType)
	// get the request
	currentRequest := requestType.GetRequestType()
	fmt.Println(currentRequest)
	fmt.Println(Login)

	switch currentRequest {
	case Login:
		ChatInfo.Print("Got request of login")
		return serverMng.loginToChatServer(request)
	case Logout:
		ChatInfo.Print("Got request of logout")
		return serverMng.logoutFromChatServer(request)
	}

	return
}

func createResponseMessage(responseType ResponseT, message *string) (response []byte, retErr error) {
	switch responseType {
	case Succeeded:
		responseTypeStruct := CreateResponseType(responseType)
		return responseTypeStruct.GetResponseJSON()
	}
	return
}

func validatePassword(login *LoginRequest, user *User) (response []byte, retErr error) {
	if strings.Compare(user.GetPassword(), login.Password) == 0 {
		user.SetUserIsConnected(true)
		ChatInfo.Print("user ", login.UserName, " is logged in")
		return createResponseMessage(Succeeded, nil)
	}

	ChatInfo.Print("user ", login.UserName, " tried to login with invalid parameter")
	message := "in correct one of the fields"
	return createResponseMessage(Failed, &message)
}

func (serverMng *ServerManager) loginToChatServer(request []byte) (response []byte, retErr error) {
	login := new(LoginRequest)
	json.Unmarshal(request, &login)
	fmt.Println(login)

	if user, ok := serverMng.mAllUsers[login.UserName]; ok {
		ChatInfo.Print("user ", login.UserName, " trying to login again")
		if user.IsUserConnected() == false {
			return validatePassword(login, user)
		}

		ChatInfo.Print("user ", login.UserName, " tried to login again")
		message := "you are login in already"
		return createResponseMessage(Failed, &message)
	}

	ChatInfo.Print("user ", login.UserName, " is not registered")
	return request, nil // createResponseMessage(Failed, &message)
}

func (serverMng *ServerManager) logoutFromChatServer(request []byte) (response []byte, retErr error) {
	logout := new(LogoutRequest)
	byteToBuffer := bytes.NewBuffer(request)
	gobObj := gob.NewDecoder(byteToBuffer)
	err := gobObj.Decode(logout)
	if err != nil {
		ChatError.Print(err)
		return nil, err
	}

	if user, ok := serverMng.mAllUsers[logout.UserName]; ok {
		if user.IsUserConnected() == true {
			user.SetUserIsConnected(false)
			ChatInfo.Print("user ", logout.UserName, "is logging out")
			return createResponseMessage(Succeeded, nil)
		}

		ChatInfo.Print("user ", logout.UserName, " tried to logout again")
		message := "you are logout already"
		return createResponseMessage(Failed, &message)
	}

	ChatInfo.Print("user ", logout.UserName, " is not registered")
	message := "in correct one of the fields"
	return createResponseMessage(Failed, &message)
}
