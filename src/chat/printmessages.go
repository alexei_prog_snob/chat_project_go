package chat

import (
	"fmt"
)

type screenInterface interface {
	printState()
	getRequestFromInput(input byte) (requestType RequestT, retErr error)
}

type mainScreen struct {
	mMessage    string
	mValidInput map[byte]RequestT
}

func createMainScreen() *mainScreen {
	return &mainScreen{
		mMessage:    "Welcome To The Chat\n\n(1) Login\n(2) Register\n(3) Exit",
		mValidInput: map[byte]RequestT{1: Login, 2: Register, 3: ClientExit}}
}

func (screen *mainScreen) printState() {
	fmt.Println(screen.mMessage)
}

func (screen *mainScreen) getRequestFromInput(input byte) (requestType RequestT, retErr error) {
	requestType, ok := screen.mValidInput[input]
	if ok == false {
		return requestType, &InvalidParamError{fmt.Sprintf("Invalid input : %d", input)}
	}

	return
}

type afterLogInScreen struct {
	mMessage string
}

func createAfterLogInScreen() *afterLogInScreen {
	return &afterLogInScreen{
		mMessage: "(1) Logout"}
}

func (screen *afterLogInScreen) printState() {
	fmt.Println(screen.mMessage)
}

func (screen *afterLogInScreen) getRequestFromInput(input byte) (requestType RequestT, retErr error) {
	return
}
