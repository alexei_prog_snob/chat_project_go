package tcp

import (
	"fmt"
)

// InvalidParamError this error return
// if you pass invalid param to tcp
type InvalidParamError struct {
	mErrorMsg string
}

func (err *InvalidParamError) Error() string {
	return fmt.Sprintf("Invalid param error: %s", err.mErrorMsg)
}

// RegisterError this error returns
// if the registration to port of client or server failed
type RegisterError struct {
	mErrorMsg string
}

func (err *RegisterError) Error() string {
	return fmt.Sprintf("Registration fail at: %s", err.mErrorMsg)
}

// ConnectionError this error returns
// if client or server fail to connect to port
type ConnectionError struct {
	mErrorMsg string
}

func (err *ConnectionError) Error() string {
	return fmt.Sprintf("Connection fail at: %s", err.mErrorMsg)
}

// CloseConnectionError this error returns
// if the close connetion of client or server failed
type CloseConnectionError struct {
	mErrorMsg string
}

func (err *CloseConnectionError) Error() string {
	return fmt.Sprintf("Failed to close connetion at: %s", err.mErrorMsg)
}

type ReadDataError struct {
	mErrorMsg string
}

func (err *ReadDataError) Error() string {
	return fmt.Sprintf("Fail to receive data in: %s", err.mErrorMsg)
}

type SendAllDataError struct {
	mErrorMsg string
	MSize     int
}

func (err *SendAllDataError) Error() string {
	return fmt.Sprintf("Fail to send all data in: %s", err.mErrorMsg)
}
