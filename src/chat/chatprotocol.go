/*
Package chat :
Copyright (c) 2019 Alexei Radashkovsky

This software is released under the MIT License.
https://opensource.org/licenses/MIT
@package:	 chat
@file name:	 chatprotocol.go
@file description:
			This file define the protocols requests and responses that both
			client and server should know.
*/
package chat

import "encoding/json"

type (
	// RequestT byte type
	RequestT byte

	// ResponseT byte type
	ResponseT byte
)

const (
	// Login : flag to server to login
	Login RequestT = iota

	// Logout : flag to server to logout
	Logout

	// Register : flag to register new user
	Register

	// ClientExit request from client to exit the client app
	ClientExit
)

type clientRequest interface {
	GetRequestType() RequestT
	GetRequestJSON() ([]byte, error)
}

/*
RequestType commom struct to all request
@name: RequestType
@description:
		After receiving massage from client need
		to cast to this struct first to receive the request
		and then operate.
*/
type RequestType struct {
	MyRequest RequestT
}

/*
GetRequestType : method of RequestType is implementation of
					interaface function
@return: RequestT - the request type
*/
func (RequestType *RequestType) GetRequestType() RequestT {
	return RequestType.MyRequest
}

/*
LoginRequest : struct
@description:
			This struct sends client to ask to login
@data methods:
			MyRequest - define the request
			UserName  - user name to login
			Password  - user's passwords
*/
type LoginRequest struct {
	MyRequest RequestT
	UserName  string `json:"UserName"`
	Password  string `json:"Password"`
}

/*
CreateLoginRequest :
@Description:
	This function is creating new LoginRequest type
	from user name and password
@params:
	userName[in] - string user name
	password[in] - string user's password
@Return:
	*LoginRequest - return new LoginRequest on success, return nill on failure
*/
func CreateLoginRequest(userName, password string) *LoginRequest {
	return &LoginRequest{
		MyRequest: Login,
		UserName:  userName,
		Password:  password}
}

/*
GetRequestType : method of LoginRequest is implementation of
					interaface function
@return: RequestT - the request type
*/
func (login *LoginRequest) GetRequestType() RequestT {
	return login.MyRequest
}

/*
GetRequestJSON : method of LoginRequest is implementation of
					interaface function
@return: RequestT - the request type
*/
func (login *LoginRequest) GetRequestJSON() ([]byte, error) {
	return json.Marshal(login)
}

/*
LogoutRequest : struct
@description:
			This struct sends client to ask to logout
@data methods:
			MyRequest - define the request
			UserName  - user name to logout
*/
type LogoutRequest struct {
	MyRequest RequestT
	UserName  string
}

/*
CreateLogoutRequest :
@Description:
	This function is creating new LogoutRequest type
	from user name and password
@params:
	userName[in] - string user name
@Return:
	*LoginRequest - return new LogoutRequest on success, return nill on failure
*/
func CreateLogoutRequest(userName string) *LogoutRequest {
	return &LogoutRequest{
		MyRequest: Logout,
		UserName:  userName}
}

/*
GetRequestType : method of LogoutRequest is implementation of
					interaface function
@return: RequestT - the request type
*/
func (logout *LogoutRequest) GetRequestType() RequestT {
	return logout.MyRequest
}

const (
	// Succeeded : flag to client to response if Succeeded
	Succeeded ResponseT = 100

	// Failed : flag to client to response if the request task is failed
	Failed ResponseT = 101
)

type responseToClient interface {
	GetResponseType() ResponseT
	GetResponseJSON() ([]byte, error)
}

/*
ResponseType commom struct to all responses
@name: ResponseType
@description:
		After receiving massage from server need
		to cast to this struct first to receive the request
		and then operate.
*/
type ResponseType struct {
	MyResponse ResponseT
}

/*
CreateResponseType :
@Description:
	This function is creating new ResponseType type
@params:
	responseType[in] - response type byte
@Return:
	*ResponseType - return new ResponseType on success, return nill on failure
*/
func CreateResponseType(responseType ResponseT) *ResponseType {
	return &ResponseType{MyResponse: responseType}
}

/*
GetResponseType : method of ResponseType is implementation of
					interaface function
@return: ResponseT - the request type
*/
func (ResponseType *ResponseType) GetResponseType() ResponseT {
	return ResponseType.MyResponse
}

/*
GetResponseType : method of ResponseType is implementation of
					interaface function
@return: ResponseT - the request type
*/
func (ResponseType *ResponseType) GetResponseJSON() ([]byte, error) {
	return json.Marshal(ResponseType)
}

/*
FailedWithMessage response struct
@name: FailedWithMessage
@description:
		This struct indicate failure and the message that user gets
		on the screen
*/
type FailedWithMessage struct {
	MyResponse     ResponseT
	MStringMessage string
}

/*
CreateFailedWithMessage :
@Description:
	This function is creating new FailedWithMessage type
@params:
	message[in] - message that will be printed to user
@Return:
	*FailedWithMessage - return new FailedWithMessage on success, return nill on failure
*/
func CreateFailedWithMessage(message string) *FailedWithMessage {
	return &FailedWithMessage{
		MyResponse:     Failed,
		MStringMessage: message}
}

/*
GetResponseType : method of FailedWithMessage is implementation of
					interaface function
@return: ResponseT - the request type
*/
func (failedWithMsg *FailedWithMessage) GetResponseType() ResponseT {
	return failedWithMsg.MyResponse
}
