package main

import (
	"bufio"
	"chat"
	"fmt"
	"os"
	"strings"
	"tcp"
)

func getStringFromUser(msg string) string {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print(msg)
	text, err := reader.ReadString('\n')
	if err != nil {
		return ""
	}
	return strings.TrimSuffix(text, "\n")
}

func main() {
	var err error
	var userRequest chat.RequestT
	fmt.Println("Client Main")

	clientToServer := tcp.CreateTCPClient()
	if clientToServer == nil {
		fmt.Println("clientToServer is nil")
		return
	}

	defer clientToServer.DestroyTCPClient()
	err = clientToServer.InitClient()
	if err != nil {
		fmt.Println(err)
		return
	}
	userInterface := chat.CreateUserInterFace()
	userInterface.PrintTheState()
	userRequest, err = userInterface.GetFromUserAnInput()
	switch userRequest {
	case chat.Login:
		fmt.Println("Please Enter User Name and Password")
		userName := getStringFromUser("user name: ")
		Password := getStringFromUser("password : ")
		myRequest := chat.CreateLoginRequest(userName, Password)
		b, _ := myRequest.GetRequestJSON()

		fmt.Println(string(b))
		clientToServer.SendData(b)
	}
}
