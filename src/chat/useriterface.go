package chat

import (
	"fmt"
)

// StateFunc :
type StateFunc func() (retErr error)

/*
UserInterface : struct
@description:
			This Struct construct the intreface of the user experience
@data members:
*/
type UserInterface struct {
	mState int
	mList  []screenInterface
}

/*
CreateUserInterFace :
@Description:
	This function create a new UserInterface
@Return:
*/
func CreateUserInterFace() *UserInterface {
	return &UserInterface{
		mState: 0,
		mList: []screenInterface{
			createMainScreen(),
			createAfterLogInScreen()}}
}

/*
PrintTheState :
@Description:
@Return:
*/
func (userIntF *UserInterface) PrintTheState() (retErr error) {
	userIntF.mList[userIntF.mState].printState()
	return
}

/*
GetFromUserAnInput :
@Description:
@Return:
*/
func (userIntF *UserInterface) GetFromUserAnInput() (requestType RequestT, retErr error) {
	var input byte
	fmt.Print(">")
	_, err := fmt.Scanf("%d", &input)
	if err != nil {
		retErr = err
		return
	}

	requestType, err = userIntF.mList[userIntF.mState].getRequestFromInput(input)

	if err != nil {
		retErr = err
		return
	}

	return
}
